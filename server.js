var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var requestJson = require('request-json') // asignamos el enlace a la biblioteca
app.use(bodyParser.json())

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancopsv/collections"
var apiKey="apiKey=mutGHka3DtwoeGIyvmEBkGqaJjXEnHvy"    // siempre al final de la url
var clienteMlab = null

app.get('/apitechu/v5/usuarios', function(req,res){
  // función que devuelve toda la colección de usuarios que hay en la BB.DD de Mongo
  clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+apiKey)
    clienteMlab.get('',function(err,resM,body){
          if (!err) {
              res.send(body)
              }
            })
})


app.get('/apitechu/v5/usuarios/:id', function(req,res){
  // función que devuelve los datos del usario que se pasa por parámetro de la colección de usuarios que hay en la BB.DD de Mongo
  var id=req.params.id
  var query='q={"id":' + id + '} &f={"first_name":1, "last_name":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + apiKey)
    clienteMlab.get('',function(err,resM,body){
          if (!err) {
              if (body.length>0){
                res.send(body[0])
              }
              else {
                res.status(404).send('Usuario no encontrado.')
              }
              }
            })
})



app.post('/apitechu/v4/login', function (req,res){
//función que realiza el login del usuario pasado por headers.
// Busca el usuario en la BB.DD y si lo encuentra pone su propiedad logged a true, devolviendo un JSON con sus datos
var email=req.headers.email
var password= req.headers.password
var query = 'q={"email":"' + email + '","password":"' + password + '"}'

console.log (query)
clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + apiKey)
  clienteMlab.get('',function(err,resM,body){
        console.log(err)
        if (!err) {
            if (body.length>0)
            { // Login OK
              console.log("vamos a actualizar", body)
              console.log("body.id ->",body[0].id)
              clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?")

              var cambio = '{"$set":{"logged":true}}'

              clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP)

              {
                console.log(errP)
              if (!errP) {
                console.log("actualizo")
              res.send({ "Login":body[0].logged,"id":body[0].id, "nombre":body[0].first_name, "apellidos":body[0].last_name})
              }
              else {res.send('usuario no cambiado')}
              }) // el segundo parámetro del put/post es el body

            }
            else {
              res.status(404).send('Usuario no encontrado.')
            }
            }
          })
})




app.post('/apitechu/v5/logout', function(req, res) {
  // función que dado un id de cliente, lo busca y si existe pone su propiedad logged a false
    var id = req.headers.id

    var query = 'q={"id":'+ id +'}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    console.log(clienteMlab)
    clienteMlab.get('', function(err, resM, body) {
        console.log (body[0])
      if (!err) {
        if (body.length == 1) // Estaba logado
        {
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":false}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"logout":"ok", "id":body[0].id})
          })

        }
        else {
          res.status(200).send('Usuario no logado previamente')
        }
      }
    })
})



app.get('/apitechu/v5/cuentas', function(req, res) {
  // devuelve un JSON con las cuentas de un cliente
    var idcliente = req.headers.idcliente
    var query = 'q={"idcliente":'+ idcliente +'} &f={"iban":1, "_id":0}'   //+ ', "logged":true}'
    console.log(query)
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
    console.log(clienteMlab)
    clienteMlab.get('', function(err, resM, body) {
        console.log (body)
      if (!err) {
          res.send(body)
        }
        else {
          res.status(300).send('Usuario no logado previamente')
        }
      })

    })

    app.get('/apitechu/v5/cuentas2', function(req, res) {
      ////////////////// Función cuentas devolviendo iban y saldo
        var idcliente = req.headers.idcliente
        var query = 'q={"idcliente":'+ idcliente +'} &f={"iban":1,"saldo":1,"_id":0}'   //+ ', "logged":true}'
        console.log(query)
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
        console.log(clienteMlab)
        clienteMlab.get('', function(err, resM, body) {
            console.log (body)
          if (!err) {

              res.send(body)

            }
            else {
              res.status(300).send('Usuario no logado previamente')
            }
          })
        })


    app.get('/apitechu/v5/movimientos', function(req, res) {
    //devuelve todos los movimientos del iban que se pasa por headers
        var iban = req.headers.iban

        var query = 'q={"iban":'+ iban +'} &f={"movimientos":1, "_id":0}'
        console.log(query)
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&l=1&" + apiKey)
        console.log(clienteMlab)
        clienteMlab.get('', function(err, resM, body) {
            console.log (body)
          if (!err) {
            if (body.length > 0) // el iban tiene movimientos
             {

                res.send(body)
            }
            else {
              res.status(300).send('La cuenta no existe')
            }
          }
          })
        })

        function checkPass(str)
          {
            //chequea el password para que tenga:
            // al menos un número, una letra minúscula y una mayúscula
            // con 6 caracteres que sean letras, números o el underscore
            var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
            return re.test(str);
          }
    function checkEmail(str)
  {
    //chequea el email para que contenga letras y números sin caracteres especiales
        var re = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    return re.test(str);
  }

          app.put('/apitechu/v5/transferencias2', function (req,res){
// dada una cuenta origen y otra destino transfiere un importe de una a otra, validando que exista la cuenta origen
// y que además su saldo es mayor que el importe a transferir.
// También se validad la cuenta destino y si todo es correcto, se resta el saldo de la cuenta origen y se suma a la destino
          var iban_origen=req.headers.iban_origen
          var iban_destino=req.headers.iban_destino
          var importe=req.headers.importe
          var saldo_origen=0
          var saldo_destino=0

          console.log("==================================================CUENTA ORIGEN====================================================")
          var query = 'q={"iban":"' + iban_origen + '"}'
          console.log ("0.1",query)
          console.log("0.2",urlMlabRaiz+ "/cuentas?"+ query + "&l=1&" + apiKey)
          clienteMlab = requestJson.createClient(urlMlabRaiz+ "/cuentas?"+ query + "&l=1&" + apiKey)
                  clienteMlab.get('',function(err,resM,body){
                  console.log(err)
                  if (!err) {
                      if (body.length>0)
                      { // Iban origen OK

                        console.log("saldo inicial ->",body[0].saldo)
                        saldo_origen=body[0].saldo-importe
                        if (saldo_origen <0){
                          console.log("NO HAY SALDO SUFICENTE EN CUENTA ORIGEN ")
                          console.log(body)
                          res.send(body)
                        }
                        // creo un nuevo movimiento
                        var movimientos=body[0].movimientos
                        var nuevo_id=movimientos[movimientos.length-1].id+1
                        console.log("nuevo id:", nuevo_id)
                        //calculo la fecha de hoy
                        var f = new Date()
                        var fecha=  f.getFullYear()+ "/" + (f.getMonth() +1) + "/" + f.getDate()
                        var nuevo_movimiento='{ "id" :' + nuevo_id + ', "fecha" : "' +fecha+ '" , "importe" :'+importe*(-1)+' , "moneda" : "EUR"}'
                        movimientos.push(JSON.parse(nuevo_movimiento))
                        console.log("Movimientos tras push:",movimientos)
                        clienteMlab = requestJson.createClient(urlMlabRaiz+ "/cuentas?")
                        var cambio = '{"$set":{"saldo":' + saldo_origen +',"movimientos":'+ JSON.stringify(movimientos) +'}}'
                        console.log("cambio:",cambio, JSON.parse(cambio))
                        console.log('?q={"iban": ' + iban_origen + '}&' + apiKey)
                        //actualizo la cuenta origen
                        clienteMlab.put('?q={"iban": "' + iban_origen + '"}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP)
                        {
                          console.log(errP)
                        if (!errP) {
                          console.log("actualizo cuenta origen")
///////////////////////////////// actualizamos cuenta destino  //////////////////////////////////////////////
console.log("==================================================CUENTA DESTINO====================================================")

                          var query = 'q={"iban":"' + iban_destino + '"}'
                          console.log ("destino:0.1",query)
                          console.log("0.2",urlMlabRaiz+ "/cuentas?"+ query + "&l=1&" + apiKey)
                          clienteMlab = requestJson.createClient(urlMlabRaiz+ "/cuentas?"+ query + "&l=1&" + apiKey)
                          clienteMlab.get('',function(err,resM,body){
                            console.log(err)
                              if (!err) {
                                  if (body.length>0)
                                    { // Iban destino OK
                                      console.log("vamos a actualizar", body)
                                      console.log("saldo inicial ->",body[0].saldo)
                                      // pasamos los saldos a integer para sumar
                                      var temporal=0
                                      temporal=parseInt(body[0].saldo)
                                      saldo_destino=parseInt(temporal)+parseInt(importe)
                                      //creamos el nuevo movimiento
                                      console.log("Saldo Destino:",saldo_destino)
                                      movimientos=body[0].movimientos
                                      console.log("Movimientos destino",movimientos)
                                      var nuevo_id=movimientos[movimientos.length-1].id+1
                                      console.log("nuevo id:", nuevo_id)
                                      f = new Date()
                                      fecha=  f.getFullYear()+ "/" + (f.getMonth() +1) + "/" + f.getDate()
                                      console.log("fecha:", fecha)
                                      var nuevo_movimiento='{ "id" :' + nuevo_id + ', "fecha" : "' +fecha+ '" , "importe" :'+importe+' , "moneda" : "EUR"}'
                                      console.log("nuevo movimientos", nuevo_movimiento)
                                      console.log("nuevo moviemiento parseado",JSON.parse(nuevo_movimiento))
                                      movimientos.push(JSON.parse(nuevo_movimiento))
                                      console.log("Movimientos tras push:",movimientos)
                                      clienteMlab = requestJson.createClient(urlMlabRaiz+ "/cuentas?")
                                      var cambio = '{"$set":{"saldo":' + saldo_destino +',"movimientos":'+ JSON.stringify(movimientos) +'}}' // la de Ángel
                                      console.log("cambio:",cambio, JSON.parse(cambio))
                                      console.log('?q={"iban": ' + iban_destino + '}&' + apiKey)
                                      //insertamos el nuevo movimiento
                                      clienteMlab.put('?q={"iban": "' + iban_destino + '"}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP)
                                      {
                                        console.log(errP)
                                      if (!errP) {
                                        console.log("actualizo cuenta destino")
                                        console.log("Saldo origen",saldo_origen, "saldo destino",saldo_destino)
                                        console.log(bodyP)

                                        res.send({ "saldoOrigen":saldo_origen, "saldoDestino":saldo_destino})
                                        }
                                      else {
                                        console.log("problema en cuenta desino ")
                                        res.status(500).send({ error: 'problema en cuenta destino' });
                                      }

                                    }); ///// Fin del put de la cuenta destino.
                                  } // fin del if body.length de del get de la cuenta
                                  else{
                                    console.log("Iban destino no encontrado")
                                    res.status(500).send({ error: 'Iban destino no encontrado' });
                                  }
                                } // fin del not error del get

                                else {
                                  console.log("no encuentro cuenta destino")
                                  res.status(500).send({ error: 'No encuentro cuenta destino' });
                                }
                            }); // fin del ges de la cuenta destino

////////////////////////////////fin de la cuenta destino //////////////////////////////////////////////////77777
                        //res.send({ "Saldo":body[0].saldo-importe})
                        }
                        else {res.status(500).send({ error: 'Iban no cambiado' });}
                        }) // el segundo parámetro del put/post es el body

                      }
                      else {
                        res.status(500).send({ error: 'Iban origen no encontrado' });
                      }
                      }
                    })
});



        app.post('/apitechu/v5/alta', function (req,res){
// da de ala un nuevo ciente con los datos que aparecen en la cabecera, chequeando que el email y la password tienen un formato correcto
// busca el último ID insertado y genera un nuevo ID para el cliente sumándo 1 al anterior.
//Previamente verificamos que no está ya dado de alta el cliente
                var email=req.headers.email
                var password= req.headers.password
                var first_name=req.headers.first_name
                var last_name=req.headers.last_name




                    if (!checkEmail(email)) {
                      res.status(307).send("email no válido")
                      return(0)
                    }

                    if (!checkPass(password)) {
                          res.status(308).send("password no válida: debe tener: al menos uno número, una letra minúscula, una mayúscula y ser caracteres: letras, números o underscore")
                          return(0)
                        }




                var query = 'q={"email":"' + email + '","password":"' + password + '"}'
                console.log (query)
                console.log(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + apiKey)
                clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + apiKey)
                  clienteMlab.get('',function(err,resM,body){
                                    console.log('Error',err)
                                    console.log("vamos a insertar", body,body.length)
                                    console.log(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + apiKey)
                        // comprobamos que el cliente no exista antes de insertarlo, si ya existe no hacemos nada y avisamos
                        if (body.length==0){
                            console.log('El cliente no existe, luego lo insertamos')
                             // Login OK

                              //Buscamos el máximo ID

                              var query = 'f={"id":1,"_id":0}&s={id:-1}&l=1'
                              console.log (query)
                              clienteMlab = requestJson.createClient(urlMlabRaiz+ "/usuarios?"+ query + "&l=1&" + apiKey)
                                clienteMlab.get('',function(err,resM,body){
                                  if (!err){
                                    var id_nuevo=parseInt(body[0].id)+1
                                    console.log('insertamos el cliente con id', id_nuevo)
                                    // el segundo parámetro del put/post es el body
                                    var cambio = '{"email":"' + email + '","id":"' + id_nuevo + '","password":"' + password + '","first_name":"' + first_name + '","last_name":"' + last_name + '"  }'
                                    console.log(cambio)

                                    console.log(JSON.parse(cambio))
                                    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?"+ apiKey)
                                    clienteMlab.post('', JSON.parse(cambio), function(errP, resP, bodyP) {
                                      console.log(errP)
                                      if (!errP){
                                        console.log("Insertado ",id_nuevo)
                                        res.send(JSON.parse(cambio))
                                      }
                                      else  {res.status(304).send(" No insertado")}
                                    })
                                    }
                                })
                          }
                          else
                              {

                              console.log('Ya existe el cliente: ', email,password)
                              res.status(305).send("Ya existe el cliente")
                              }
                          })
                        })




app.post('/apitechu/v5/cuentainicial', function (req,res){
//creamos una cuenta inicial por defecto con un saldo también inicial y un movimiento inicial
// el iban de la cuenta lleva el ID  del cliente para facilitar la operativa posterior
        var idcliente=req.headers.idcliente
        console.log('insertamos una cuenta inicial para el cliente con id', idcliente)
        var f = new Date()
        var fecha=  f.getFullYear()+ "/" + (f.getMonth() +1) + "/" + f.getDate()
        var cambio = '{"iban":"CUENTA POR DEFECTO - ' + idcliente +'", "idcliente":' + idcliente + ',"movimientos":  [ { "id" : 1 , "fecha" : " '+  fecha + '" , "importe" : 1000.0 , "moneda" : "EUR"}], "saldo":1000}'
        console.log(cambio)

        console.log(JSON.parse(cambio))
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?"+ apiKey)
        clienteMlab.post('', JSON.parse(cambio), function(errP, resP, bodyP) {
        console.log(errP)
              if (!errP){
                                console.log("Insertada cuenta: ",cambio)
                                res.send(JSON.parse(cambio))
              }
              else
                {
                  console.log("error al crear cuenta inicial")
                  res.status(305).send("Error al crear cuenta inicial")
                }
                  })
                })


var port = process.env.PORT || 3000
var usuarios = require('./usuarios.json')
var cuentas = require('./cuentas.json')
var fs = require('fs');


console.log("Hola Mundo y mas")

app.listen(port)

console.log("API escuchando en el puerto " + port)

app.get('/apitechu/v1', function(req,res) {
//console.log(req)
res.send({"mensaje":"Bienvenido a mi API"})

})

app.get('/apitechu/v1/usuarios', function(req,res){
res.send(usuarios)

})



app.post('/apitechu/v1/usuarios', function (req,res){

var nuevo={"first_name":req.headers.first_name,
          "country":req.headers.country
}
usuarios.push(nuevo)
console.log(req.headers)
const datos = JSON.stringify(usuarios)
fs.writeFile("./usuarios.json",datos,"utf8",function(err){
if (err) console.log(err)
else console.log("fichero guardado")

})
res.send("Alta OK")


})

app.delete('/apitechu/v1/usuarios/:elegido', function(req,res){

  usuarios.splice(req.params.elegido-1,1)  // elimina elementos de un array desde la posición "id-1", 1 elemento

  res.send("usuario borrado")
})

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req,res){

console.log("Parámetros")
console.log(req.params)
console.log("Query Strings")
console.log(req.query)
console.log("headers")
console.log(req.headers)
console.log("Body")
console.log(req.body)
res.send("Alta OK")


})

app.post('/apitechu/v2/usuarios', function (req,res){

//var nuevo={"first_name":req.headers.first_name,
//          "country":req.headers.country }

var nuevo=req.body
usuarios.push(nuevo)

const datos = JSON.stringify(usuarios)
fs.writeFile("./usuarios.json",datos,"utf8",function(err){
if (err) console.log(err)
else console.log("fichero guardado")

})
res.send("Alta OK")


})

app.post('/apitechu/v2/login', function (req,res){


var idUsuario=0
var email=req.headers.email
var password= req.headers.password

for (var i = 0; i < usuarios.length; i++) {
  if (email== usuarios[i].email && password==usuarios[i].password)
  {
    idUsuario=usuarios[i].id
    usuarios[i].logged=true
    console.log("logado", usuarios[i].logged)
    break;
  }

}
console.log(idUsuario)
res.send("OK")


})

app.post('/apitechu/v2/logout', function (req,res){

var idUsuario=req.headers.id

for (var i = 0; i < usuarios.length; i++) {
  if (idUsuario == usuarios[i].id && usuarios[i].logged== true)
  {

    usuarios[i].logged=false
    console.log("usuario ",usuarios[i].id," desconectado")
    break;
  }

}


res.send("OK")


})


////// API cuentas

app.get('/apitechu/v3/cuentas', function(req,res){
var iban=req.headers.iban
var salida=[]
console.log(iban)
if (iban == null) { // si es no se ha pasado ningún iban devolvemos el fichero completo

//  res.send(cuentas) // para devolver el fichero completo

for (var i = 0; i < cuentas.length; i++) {
  salida[i]=cuentas[i].iban

  }
  res.send(salida)
}


else  // si pasamos el iban, devolvemos sus movimientos
{
for (var i = 0; i < cuentas.length; i++) {
  if (cuentas[i].iban==iban) {
    console.log("encontrado")
    res.send(cuentas[i].movimientos)
    break;
  }
}


}


})


app.get('/apitechu/v3/cliente', function(req,res){
var idcliente=req.headers.idcliente
var salida=[]
console.log(idcliente)
if (idcliente == null) {

//  si el cliente es null, devuelvo todos los clientes

for (var i = 0; i < cuentas.length; i++) {
  salida[i]=cuentas[i].idcliente

  }
  res.send(salida)
}


else  //si el cliente no es null devuelvo sus cuentas, supongo que un cliente puede tener más de una cuenta
{
var j=0
for (var i = 0; i < cuentas.length; i++) {
  if (cuentas[i].idcliente==idcliente) {
    salida[j]=cuentas[i]
    j=j+1
  }
}
res.send(salida)

}


})
