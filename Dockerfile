#Imagen raíz
FROM node
#carpeta raíz
WORKDIR /apitechu
#Copia de archivos
ADD . /apitechu
#Instalo los paquetes necesarios
RUN npm install
#volumen de la imagen
VOLUME ["/logs"]
#Puerto que expone
EXPOSE 3000
#comando de inicio
CMD ["npm", "start"]
